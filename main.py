import argparse
import math
import sys
import time
import threading
from signal import signal, SIGINT
import pybullet as p
import pypot.dynamixel
from onshape_to_robot.simulation import Simulation
from utils_corrige import SimpleRobotSimulation, SimpleRobot
from kinematics import computeIK, computeIKOriented, triangle
from constants import PHANTOMX_SIMULATION, PHANTOMX

class Main:
    def __init__(self):
        self.mode_hexapode = ""
        self.robot = None
        self.parse_arguments()
        self.perform_mode_action()

    def perform_mode_action(self):
        self.bool_stop, self.bool_leg_move_mode, self.bool_center_move_mode, self.bool_walk_mode, self.bool_rotate_mode = False, False, False, False, False
        try :
            while True:
                robot_pose = (
                    self.robot.sim.getRobotPose()
                )
                self.robot.sim.lookAt(robot_pose[0])
                try :
                    self.x = float(p.readUserDebugParameter(self.sliders["x"]))
                    self.y = float(p.readUserDebugParameter(self.sliders["y"]))
                    self.z = float(p.readUserDebugParameter(self.sliders["z"]))
                    self.leg_number = int(p.readUserDebugParameter(self.sliders["leg_number"]))
                    self.angles = float(p.readUserDebugParameter(self.sliders["angles"]))
                    self.walk_mode = int(p.readUserDebugParameter(self.buttons["walk"]))
                    self.rotate_mode = int(p.readUserDebugParameter(self.buttons["rotate"]))
                    self.center_move_mode = int(p.readUserDebugParameter(self.buttons["center_move"]))
                    self.leg_move_mode = float(p.readUserDebugParameter(self.buttons["leg_move"]))
                    self.stop = int(p.readUserDebugParameter(self.buttons["stop"]))
                    self.reset = int(p.readUserDebugParameter(self.buttons["reset"]))
                    self.step_height = float(p.readUserDebugParameter(self.sliders["step_height"]))
                    self.step_width = float(p.readUserDebugParameter(self.sliders["step_width"]))
                except Exception as e:
                    print(f"exception : {e}")
                    continue
                
                
                
                print(f"x: {self.x}, y: {self.y}, z: {self.z}, leg_number: {self.leg_number}, angles: {self.angles}, walk_mode: {self.walk_mode}, rotate_mode: {self.rotate_mode}, center_move_mode: {self.center_move_mode}, leg_move_mode: {self.leg_move_mode}, reset: {self.reset}, step_height: {self.step_height}, step_width: {self.step_width}")
                # x: 0.0, y: 0.0, z: 0.0, leg_number: 1.0, angles: 0.0, walk_mode: 1.0, rotate_mode: 1.0, center_move_mode: 1.0, leg_move_mode: 1.0, reset: 1.0, step_height: 0.009999999776482582, step_width: 0.009999999776482582
                
                if self.walk_mode %2 == 1:
                    self.bool_walk_mode = False
                else: 
                    self.bool_walk_mode = True
                    
                if self.bool_rotate_mode %2 == 1:
                    self.bool_rotate_mode = False
                else:
                    self.bool_rotate_mode = True
                    
                if self.center_move_mode %2 == 1:
                    self.bool_center_move_mode = False
                else:
                    self.bool_center_move_mode = True
                    
                if self.leg_move_mode %2 == 1:
                    self.bool_leg_move_mode = False
                else:
                    self.bool_leg_move_mode = True
                
                print(f"bool_leg_move_mode: {self.bool_leg_move_mode}, bool_center_move_mode: {self.bool_center_move_mode}, bool_walk_mode: {self.bool_walk_mode}, bool_rotate_mode: {self.bool_rotate_mode}")
                
                    
                if self.bool_leg_move_mode:
                    self.bool_rotate_mode, self.bool_center_move_mode, self.bool_walk_mode = False, False, False
                    coord = [self.x, self.y, self.z]
                    self.leg_move(coord, self.leg_number)
                    
                if self.bool_center_move_mode:
                    self.bool_rotate_mode, self.bool_leg_move_mode, self.bool_walk_mode = False, False, False
                    self.center_move_mode = 0
                    self.center_move([self.x, self.y, self.z])
                    
                if self.bool_walk_mode:
                    self.bool_rotate_mode, self.bool_center_move_mode, self.bool_leg_move_mode = False, False, False
                    self.walk(self.angles)
                
                if self.bool_rotate_mode:
                    self.bool_center_move_mode, self.bool_leg_move_mode, self.bool_walk_mode = False, False, False
                    self.rotate()
                 
        except KeyboardInterrupt:
            print("Keyboard interrupt detected. Performing mode action.")
            self.perform_mode_action()
        except Exception as e:
            print(f"Error: {e}")




    def get_position_input(self):
        position = []
        for coord in ['x', 'y', 'z']:
            value = input(f"Enter the position {coord}: ").strip()
            position.append(float(value) if value.replace('.', '', 1).isdigit() else 0)
        return position

    def get_integer_input(self, prompt, default):
        value = input(prompt).strip()
        return int(value) if value.isdigit() else default

    def get_float_input(self, prompt, default):
        value = input(prompt).strip()
        return float(value) if value.replace('.', '', 1).isdigit() else default

    def parse_arguments(self):
        parser = argparse.ArgumentParser(description="Available modes: 1: simulation, 2: real, 3: simulation and real")
        parser.add_argument("--mode", "-m", type=str, default="simulation", help="Available modes: 1: simulation, 2: real, 3: simulation and real")
        args = parser.parse_args()
        mode = args.mode.lower()
        case_robot_type = {"simulation": "1", "1": "1", "real": "2", "2": "2"}
        self.mode_hexapode = case_robot_type.get(mode, "")

        if self.mode_hexapode == "1":
            robotPath = self.parsed_argument()
        if self.mode_hexapode == "2":
            self.parsed_argument(robotPath)
        self.buttons = {}

        self.sliders = {"x": p.addUserDebugParameter("x", -0.2, 0.2, 0)}
        self.sliders["y"] = p.addUserDebugParameter("y", -0.2, 0.2, 0)
        self.sliders["z"] = p.addUserDebugParameter("z", -0.2, 0.2, 0)
        self.sliders["angles"] = p.addUserDebugParameter("roll", -math.pi, math.pi, 0)
        self.sliders["step_height"] = p.addUserDebugParameter("step_height", 0.01, 0.1, 0.01)
        self.sliders["step_width"] = p.addUserDebugParameter("step_width", 0.01, 0.1, 0.01)
        self.sliders["leg_number"] = p.addUserDebugParameter("leg_number", 1, 6, 1)

        self.buttons["walk"] = p.addUserDebugParameter("walk", 1, 0, 0)
        self.buttons["rotate"] = p.addUserDebugParameter("rotate", 1, 0, 0)
        self.buttons["center_move"] = p.addUserDebugParameter("center_move", 1, 0, 0)
        self.buttons["leg_move"] = p.addUserDebugParameter("leg_move", 1, 0, 0)
        self.buttons["reset"] = p.addUserDebugParameter("reset", 1, 0, 0)
        self.buttons["stop"] = p.addUserDebugParameter("stop", 1, 0, 0)
            

    def parsed_argument(self, robotPath):
        sim = Simulation(robotPath, gui=True, panels=True, useUrdfInertia=False)
        ROBOT_TYPE = PHANTOMX
        self.simulation = False
        ports = pypot.dynamixel.get_available_ports()
        if not ports:
            raise IOError('No port found!')
        print("Connecting on the first available port:", ports[0])
        dxl_io = pypot.dynamixel.DxlIO(ports[0], baudrate=1000000)
        self.robot = SimpleRobot(dxl_io)
        self.robot.enable_torque()
        self.robot.smooth_tick_read_and_write(1)
        signal(SIGINT, self.shutdown())


    def parsed_argument(self):
        ROBOT_TYPE = PHANTOMX_SIMULATION
        self.simulation = True

        result = "phantomx_description/urdf/phantomx.urdf"
        sim = Simulation(result, gui=True, panels=True, useUrdfInertia=False)
        pos, rpy = sim.getRobotPose()
        sim.setRobotPose([0, 0, 0.5], [0, 0, 0, 1])
        self.robot = SimpleRobotSimulation(sim)
        
        self.robot.centerCamera = True
        self.robot.init()
        self.robot.enable_torque()
        signal(SIGINT, self.shutdown)

        return result
        

    def shutdown(self, signal_received, frame):
        print("SIGINT or CTRL-C detected. Setting motors to compliant and exiting")
        if self.robot:
            self.robot.disable_torque()
        if frame:
            print(f"frame: {frame}")
        if signal_received:
            print(f"signal_received: {signal_received}")
        print("Done ticking. Exiting.")
        sys.exit()


    def leg_move(self, position, leg_number):
        try:
            angles = computeIK(position[0], position[1], position[2])
            for j in range(3):
                self.robot.legs[leg_number][j].goal_position = angles[j]

        except ValueError:
            print(f"error : {ValueError}")
            

    def center_move(self, position):
        try:
            for i in [1, 3, 5]:
                angles = computeIKOriented(-position[0], -position[1], -position[2], i, 0, self.simulation)
                for j in range(3):
                    self.robot.legs[i][j].goal_position = angles[j]
                    
            for i in [2, 4, 6]:
                angles = computeIKOriented(-position[0], -position[1], -position[2], i, 0, self.simulation)
                for j in range(3):
                    self.robot.legs[i][j].goal_position = angles[j]
            
            self.robot.tick_read_and_write()
            if self.mode_hexapode == "1":
                self.robot.centerCamera = True
                self.robot.sim.tick()   
                
        except ValueError:
            print(f"error : {ValueError}")


    def walk(self, direction):
        self.move()


    def rotate(self):
        self.move(oriented = False)

  
  
    def move(self, oriented = True):
        try:

            
            for i in [1, 3, 5]:
                angles = triangle(0, 0, self.step_height, self.step_width, time.time()*0.5, self.angles, oriented, i, self.simulation)
                for j in range(3):
                    self.robot.legs[i][j].goal_position = angles[j]
                    
            for i in [2, 4, 6]:
                angles = triangle(0, 0, self.step_height, self.step_width, time.time() * 0.5 + 0.5, self.angles, oriented, i, self.simulation)
                for j in range(3):
                    self.robot.legs[i][j].goal_position = angles[j]

            self.robot.tick_read_and_write()
            if self.mode_hexapode == "1":
                self.robot.centerCamera = True
                self.robot.sim.tick()

            
        except KeyboardInterrupt:
            print("Keyboard interrupt detected. Performing mode action.")
            self.perform_mode_action()
            
        except Exception as e:
            print(f"Error: {e}")



if __name__ == "__main__":
    main = Main()



            

