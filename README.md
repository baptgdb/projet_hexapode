# compte rendu du projet hexapode de robotique mobile 

## Equipe :
    - GADEBILLE Baptiste
    - LOPEZ Tao
    - TARAIRE Lucas
    - RANDONNET Bertil

- Répartition des tâches :
    - Baptiste : 
          - Simulation (Une patte, Marche, Corps, Tourner)
          - Architecture finale

    - Bertil : 
          - Réel (Tourner, Marcher)
          - Compte rendu/Présentation

    - Tao : 
          - Simulation (Une patte, Marche, Tourner)
          - Présentation


    - Lucas : 
          - Réel (Tourner, Marcher)



## Bonus si on a le temps :

    - Combinaison de rotation et de marche en ligne droite. Marche holonomique.

    - Les paramètres de marche peuvent être modifiés en déplacement (fréquence des pas, amplitude des pas, hauteur des pas, position par défaut du robot, etc.)

    - Allez dans (x, y, thêta). Le robot peut atteindre n’importe quelle position cible (x, y, thêta) en y marchant.

    - Les jambes bougent en douceur (faites mieux que l'interpolation à vitesse constante). Mots-clés : accélération, bang-bang, min-jerk, feed-forward.

    - Robot non rattaché. Vous pouvez utiliser la batterie et le tableau de contrôle de niveau bas.

    - Danse.
    
    - Dessiner quelque chose avec un crayon au bout d'une patte.

## architecture logiciel :

Architecture en début de projet :
 - fichier main.py
   - classe main() : lance les autres modes : hexapode, simulation
   - 

 - hexapode.py et simulation.py ont les mêmes constantes et fonctions de cinematique.

 Architecture finale : 
  - main.py
    -lance simulation et hexapode en même temps 


## diagramme UML



![Diagramme_UML](https://hackmd.io/_uploads/BkBScBC9T.png)


les classes sont les suivantes :
- main

les fonctions de la classe **main**

fonctions communes a hexapode et simulation :
  - **perform_mode_action(self):**
  - **get_position_input(self): **
  - **get_integer_input(self, prompt, default):**
  - **get_float_input(self, prompt, default):**
  - **parse_arguments(self):**
  - **parsed_argument(self, robotPath): **
  - **shutdown(self, signal_received, frame): **
  - **leg_move(self, position, leg_number):**
  - **center_move(self, position): **
  - **walk(self, direction): **
  - **rotate(self):**
  - **move(self, oriented = True): **



"""
**Comportement obligatoire:**


<span style="color:green">**SIMULATION :**</span>

- [x]  - Déplacer une patte arbitraire vers une position arbitraire (x, y, z)
- [x]  - Déplacer le centre du robot vers une position arbitraire (x, y, z) (les 6 jambes restant au sol)
- [x]  - Marcher en ligne droite (direction arbitraire)
- [x]  - Rotation sans déplacer le centre du robot 

<span style="color:darkred">**REEL :**</span> 

- [ ]  - Déplacer une patte arbitraire vers une position arbitraire (x, y, z)
- [ ]  - Déplacer le centre du robot vers une position arbitraire (x, y, z) (les 6 jambes restant au sol)
- [x]  - Marcher en ligne droite (direction arbitraire)
- [x]  - Rotation sans déplacer le centre du robot 

**Comportement avancée :**

- Combinaison de rotation et de marche en ligne droite. Marche holonomique.

- Contrôler le robot avec un clavier/souris/quoi que ce soit

- Les paramètres de marche peuvent être modifiés en déplacement (fréquence des pas, amplitude des pas, hauteur des pas, position par défaut du robot, etc.)

- Odométrie. Être capable de suivre la position (x, y, thêta) du robot pendant qu'il marche (bien sûr, la position de départ est considérée comme connue)

- Allez dans (x, y, thêta). Le robot peut atteindre n’importe quelle position cible (x, y, thêta) en y marchant.

- Les jambes bougent en douceur (faites mieux que l'interpolation à vitesse constante). Mots-clés : accélération, bang-bang, min-jerk, feed-forward.

- Le robot peut changer de direction en déplacement

- Robot non attaché. Vous pouvez utiliser la batterie et le tableau de contrôle de niveau bas

"""
